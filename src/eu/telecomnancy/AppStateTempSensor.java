package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TempSensorState;
import eu.telecomnancy.ui.ConsoleUI;

public class AppStateTempSensor {
	
    public static void main(String[] args) {
        ISensor sensor = new TempSensorState();
        new ConsoleUI(sensor);
    }

}
