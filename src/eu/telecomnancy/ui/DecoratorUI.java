package eu.telecomnancy.ui;


import eu.telecomnancy.sensor.FahrConvertDecorator;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.RoundConvertDecorator;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DecoratorUI {

    private ISensor sensor, oldSensor;
    
    private Scanner console;

    public DecoratorUI(ISensor sensor) {
        this.sensor = sensor;
        this.oldSensor = sensor;
        this.console = new Scanner(System.in);
        manageCLI();
    }

    public void manageCLI() {
        String rep = "";
        System.out.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value - f: fahrenheit - r: round");
        
        boolean f, r;
        f = false;
        r = false;
        
        while (!"q".equals(rep)) {
            try {
                System.out.print(":> ");
                rep = this.console.nextLine();
                if ("on".equals(rep) || "o".equals(rep)) {
                    this.sensor.on();
                    System.out.println("sensor turned on.");
                } else if ("off".equals(rep) || "O".equals(rep)) {
                    this.sensor.off();
                    System.out.println("sensor turned off.");
                } else if ("status".equals(rep) || "s".equals(rep)) {
                    System.out.println("status: " + this.sensor.getStatus());
                } else if ("update".equals(rep) || "u".equals(rep)) {
                    this.sensor.update();
                    System.out.println("sensor value refreshed.");
                } else if ("value".equals(rep) || "v".equals(rep)) {
                    System.out.println("value: " + this.sensor.getValue());
                } else if("r".equals(rep)){
                	if(r){
                		this.sensor = oldSensor;
                		r = false;
                    	System.out.println("Not Rounded");
                	}else{
                		this.sensor = new RoundConvertDecorator(sensor);
                		r = true;
                    	System.out.println("Rounded ON");
                	}
                }else if ("f".equals(rep)){                	
                	if (f){
                		this.sensor = oldSensor;
                		f = false;
                    	System.out.println("Celsius ON");
                	}else{
                		this.sensor = new FahrConvertDecorator(sensor);
                		f = true;
                    	System.out.println("farhenheit ON");     	
                   	}
                }else {
                    System.out.println("quit|q: quitter - on|o: switch - off|O: switch - status|s: status - update|u: refresh - value|v: value");
                }
            } catch (SensorNotActivatedException ex) {
                Logger.getLogger(ConsoleUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
