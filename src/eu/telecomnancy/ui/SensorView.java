package eu.telecomnancy.ui;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.Command;
import eu.telecomnancy.sensor.CommandeGetValue;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.CommandeOff;
import eu.telecomnancy.sensor.CommandeOn;
import eu.telecomnancy.sensor.CommandeUpdate;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

public class SensorView extends JPanel implements Observer{
	
	private ISensor sensor;
    private Command cmde;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    
    private JMenuBar menuBar = new JMenuBar();
    private JMenu menu = new JMenu("Actions");
    

    public SensorView(ISensor c) {
        this.sensor = c;
        ((TemperatureSensor)this.sensor).addObserver(this);
        
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);
        
        //-----------------------------------------
        //get the commands from properties file        
        
        ReadPropertyFile rp=new ReadPropertyFile();
        Properties p;
        try {
			p=rp.readFile("/eu/telecomnancy/commande.properties");		
			
			JMenuItem tmp;
			String aCommand;			
			
	        for (String i: p.stringPropertyNames()) {
	            aCommand = p.getProperty(i);
	            tmp = new JMenuItem(aCommand.substring(16));
	            
	            if (aCommand.equals("eu.telecomnancy.CommandeOn")){
            		 
	            	tmp.addActionListener(new ActionListener() {
     	                public void actionPerformed(ActionEvent e) {
     	                	cmde = new CommandeOn(sensor);
     	                	cmde.execute();
     	                }
     	            });
            	}
	            else if (aCommand.equals("eu.telecomnancy.CommandeOff")){
            		
            		tmp.addActionListener(new ActionListener() {
     	                public void actionPerformed(ActionEvent e) {
     	                	cmde = new CommandeOff(sensor);
     	                	cmde.execute();
     	                }
     	            });
            	}
	            else if (aCommand.equals("eu.telecomnancy.CommandeUpdate")){
            		
            		tmp.addActionListener(new ActionListener() {
     	                public void actionPerformed(ActionEvent e) {
     	                	cmde = new CommandeUpdate(sensor);
     	                	cmde.execute();
     	                }
     	            });
            	}
	            else if (aCommand.equals("eu.telecomnancy.CommandeGetValue")){
            		
            		tmp.addActionListener(new ActionListener() {
     	                public void actionPerformed(ActionEvent e) {
     	                	cmde = new CommandeGetValue(sensor);
     	                	cmde.execute();
     	                }
     	            });
            	}	                      
	            menu.add(tmp);
	        }	        
	        menuBar.add(menu);	        
		} catch (IOException e1) {			
			e1.printStackTrace();
		}
        
        //------------------------------------------------------
        
        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	cmde = new CommandeOn(sensor);
            	cmde.execute();
            }
        });      	
        
        
        
        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cmde = new CommandeOff(sensor);
                cmde.execute();                
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                	cmde = new CommandeUpdate(sensor);
                	cmde.execute();
            }
        });    	
    	

        

        

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
        this.add(menuBar, BorderLayout.NORTH);
        
    }

	@Override
	public void update(Observable o, Object refresh) {
		this.sensor = ((ISensor)o);

		if (sensor.getStatus()){				
			try{
				String txt=""+sensor.getValue();
				this.value.setText(txt);
			}catch (SensorNotActivatedException e) {
				this.value.setText("Activate Sensor First");
			}
		}else{
			this.value.setText("Sensor Off");
		}			
		
	}
}
