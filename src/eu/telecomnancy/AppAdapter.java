package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegTempAdapter;
import eu.telecomnancy.ui.ConsoleUI;

public class AppAdapter {

	 public static void main(String[] args) {
	        ISensor sensor = new LegTempAdapter();
	        new ConsoleUI(sensor);
	    }
	
}
