package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.DecoratorUI;

public class AppDecorator {
    public static void main(String[] args) {
        ISensor sensor = new TemperatureSensor();
        new DecoratorUI(sensor);
    }
}
