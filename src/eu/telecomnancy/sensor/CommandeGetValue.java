package eu.telecomnancy.sensor;

public class CommandeGetValue implements Command{
	
	private ISensor sensor;
	
	public CommandeGetValue(ISensor newSensor){
		sensor = newSensor;
	}
	
	public void execute() {
		sensor.off();
		
	}
}
