package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOn implements State{

	private boolean status = true;
	private double value;


	@Override
	public boolean getStatus() {
		return status;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		this.value = (new Random()).nextDouble() * 100;		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return value;
	}
	
	

}
