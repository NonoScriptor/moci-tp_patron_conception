package eu.telecomnancy.sensor;

public class FahrConvertDecorator extends ISensorDecorator{
	
	
	public FahrConvertDecorator(ISensor s){
		super(s);
	}
	
	public double getValue() throws SensorNotActivatedException {
		double value = super.getValue();
		return (value*1.8 + 32);		
	}
	
		
	
	

}
