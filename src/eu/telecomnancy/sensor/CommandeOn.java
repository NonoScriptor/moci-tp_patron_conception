package eu.telecomnancy.sensor;

public class CommandeOn implements Command{

	private ISensor sensor;
	
	public CommandeOn(ISensor newSensor){
		sensor = newSensor;
	}
	
	public void execute() {
		sensor.on();
		
	}
	
}
