package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOff implements State{

	private boolean status = false;

	@Override
	public boolean getStatus() {
		return status;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");	
	}
	

}
