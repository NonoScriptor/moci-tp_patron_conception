package eu.telecomnancy.sensor;

public class CommandeUpdate implements Command{

	private ISensor sensor;
	
	public CommandeUpdate(ISensor newSensor){
		sensor = newSensor;
	}
	
	public void execute() {
	
		try{
			sensor.update();
		}catch (SensorNotActivatedException e){
			e.printStackTrace();
		}		
	}
	
}



