package eu.telecomnancy.sensor;

public class TempSensorState implements ISensor{

	public State sensorState;
	public double value;
	
	public void setState(State s){
		this.sensorState = s;
	}
	
	@Override
	public void on() {
		this.setState(new StateOn());
		
	}

	@Override
	public void off() {
		this.setState(new StateOff());
		
	}

	@Override
	public boolean getStatus() {
		return this.sensorState.getStatus();
		
	}

	@Override
	public void update() throws SensorNotActivatedException {
		try{
			this.sensorState.update();
		}catch(SensorNotActivatedException e){
			e.printStackTrace();
		}
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		this.value = this.sensorState.getValue();
		return this.value;
	}
	
	

}
