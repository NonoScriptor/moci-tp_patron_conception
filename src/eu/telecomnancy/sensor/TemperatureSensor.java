package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import eu.telecomnancy.ui.SensorView;

public class TemperatureSensor extends Observable implements ISensor{
    boolean state, refresh;
    double value = 0;
    Observer vue;

    @Override
    public void on() {    	
        state = true;
        if (this.vue != null)
        	this.notifyObservers();
    }

    @Override
    public void off() {
    	state = false;
    	if (this.vue != null)
    		this.notifyObservers();
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state){
            value = (new Random()).nextDouble() * 100;
            this.refresh = true;
            if (this.vue != null)
            	this.notifyObservers();
            this.refresh = false;
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
    
    public void addObserver(Observer v){
		this.vue = v;
	}
	
	public void deleteObserver(Observer v){
		if (this.vue != null)
			this.vue = null;
	}
	
	public void notifyObservers(){
		System.out.println("Notifier vues");		
		this.vue.update(this, this.refresh);
	}
    

}
