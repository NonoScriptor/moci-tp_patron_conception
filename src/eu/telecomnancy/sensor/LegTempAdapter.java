package eu.telecomnancy.sensor;

public class LegTempAdapter implements ISensor{
	private LegacyTemperatureSensor lgt;
	
	public LegTempAdapter(){
		this.lgt = new LegacyTemperatureSensor();
	}
		
	@Override
	public void on() {
		this.lgt.onOff();
	}

	@Override
	public void off() {
		this.lgt.onOff();		
	}

	@Override
	public boolean getStatus() {
		return this.lgt.getStatus();		
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (!this.getStatus()){
			throw new SensorNotActivatedException("Sensor must be activated before updating new values.");
		}else{
			//do nothing cause a thread is doing stuff
			
		}		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		
		if(this.getStatus()){
			return this.lgt.getTemperature();
		}else{
			throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
			
		}
		
				
	}
	

}
