package eu.telecomnancy.sensor;

public class ISensorDecorator implements ISensor{

	private ISensor sensor;
	
	public ISensorDecorator(ISensor s){
		this.sensor = s;
	}
	
	@Override
	public void on() {
		this.sensor.on();
		
	}

	@Override
	public void off() {
		this.sensor.off();
		
	}

	@Override
	public boolean getStatus() {
		return this.sensor.getStatus();
		
	}

	@Override
	public void update() throws SensorNotActivatedException {
		this.sensor.update();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return this.sensor.getValue();
	}
	

}
