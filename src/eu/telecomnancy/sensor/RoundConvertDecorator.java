package eu.telecomnancy.sensor;

public class RoundConvertDecorator extends ISensorDecorator{
	
	public RoundConvertDecorator(ISensor s){
		super(s);
	}

	public double getValue() throws SensorNotActivatedException {
		double value = super.getValue();
		return (Math.round( value * 100.0 ) / 100.0);	
	}
}
